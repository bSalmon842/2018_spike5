﻿---
Spike Report
---

Core Spike 6
============

Introduction
------------

Building from the small level created in Core Spike 5, more visual
elements are added. Such as a Main Menu, a Pause Menu, and a HUD that
makes use of the UMG UI Designer.

Bitbucket Link: <https://bitbucket.org/bSalmon842/2018_spike5> (Uses the same
repository and project as Core Spike 5)

Goals
-----

Building on Core Spike 5 (C++ and Unreal Engine), add:
	
	•	A Heads-Up Display. You can do this in one of several ways:
		
		1.	[Preferred] In your custom PlayerController, create your Widget and attach it

		2.	[Rudimentary] In the Level Blueprint, create/attach the Widget to Player Controller 0

		3.	[Depreciated] Add a custom HUD class to your custom GameMode
		
	•	Link the HUD to your player pawn (C++) by displaying some information about the player pawn
	
		•	[It’s up to you – eg. adding a number of bullets, or health?]
		
	•	A Main Menu, in a separate scene, which has 3 buttons:
	
		•	Start a New Game

		•	Show some Info (Gameplay controls? Credits?)

		•	Or Quit the game
		
	•	A Pause Menu, during which:

		•	The game pauses, and the mouse is shown (if hidden)

		•	The player can Return to the Game (re-hides the mouse)

		•	The player can Go to the Menu (change scene)


Personnel
---------

  ------------------------- ------------------
  Primary -- Brock Salmon   Secondary -- N/A
  ------------------------- ------------------

Technologies, Tools, and Resources used
---------------------------------------

-   <https://docs.unrealengine.com/latest/INT/Engine/UMG/QuickStart/index.html>

-	https://wiki.unrealengine.com/UMG,_Referencing_UMG_Widgets_in_Code

Tasks undertaken
----------------

1.  Create a Widget Blueprint for the HUD

2.	In the Player Character Class, create a new variable that is exposed
	to Blueprints called 'shotsFired', and increment it every time the
	player's weapon is fired.
	
3.	Setup the HUD for presentation and create a Text Variable in the
	HUD, in the blueprint of the widget bind the text to the Player
	Character's shotsFired value.
	
4.	In the level blueprint, attach the widget to the viewport of the player.
	Also set the Input Mode to Game Only for later.

5.  Create a New Level for the Main Menu that will remain blank

6.  Create a Widget Blueprint to design and give functionality to the Menu
	Level. In this case there are 3 buttons to Play, Show the Controls, and
	Quit the Game.

7.  Create a Widget Blueprint for the Main Menu, in it create the design
    and then set each button to have the correct functionality. The Mouse
	Cursor should be hidden when the user clicks Play.
	
8.	In the Main Menu level blueprint, set the Widget the appear and enable
	the mouse cursor.
	
9.	In the Project Settings, change the default Game Startup Map to be the
	Main Menu level.

10. Create a Widget Blueprint for the Pause Menu and set it up with a Resume
	and Quit to Menu button.

11. In the Player Character blueprint use a Key event to make the Pause Menu
	appear (in this case the M key was used). Also make the Mouse Cursor
	re-appear for the Pause Menu.
	
12. In the Player Character after the Pause menu has been enabled set the
	Input Mode to UI Only

What we found out
-----------------

From this Spike we learned how to use the UMG UI Designer to create Menu
systems and a Heads-Up Display, Unreal Engine has several ways to create
User Interfaces. Using UMG we also learned how to attach these elements
via Widget, Level, and Player Character Blueprints. Using the Designer
a whole range of User Interfaces can be produced and be working in game
quickly, which is vital for prototyping, and the system is extensible enough
for more polished interfaces.
