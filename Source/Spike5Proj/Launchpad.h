// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Launchpad.generated.h"

UCLASS()
class SPIKE5PROJ_API ALaunchpad : public AActor
{
	GENERATED_BODY()
	
public:	
	UPROPERTY(VisibleAnywhere, Category = "Launchpad Components")
	class UMaterial *meshMaterial;

	UPROPERTY(VisibleAnywhere, Category = "Launchpad Components")
	class UStaticMeshComponent *meshComp;
	
	UPROPERTY(VisibleAnywhere, Category = "Launchpad Components")
	class UBoxComponent *boxComp;

	UPROPERTY(VisibleAnywhere, Category = "Launchpad Components")
	class UArrowComponent *arrowComp;

	UPROPERTY(VisibleAnywhere, Category = "Launchpad Components")
	class USoundBase *soundBase;

	UPROPERTY(EditAnywhere, Category = "Launchpad Components")
	float launchVelocity;

	ALaunchpad();

	UFUNCTION()
	void OnOverlapBegin(class UPrimitiveComponent* overlappedComp,
						class AActor *otherActor, class UPrimitiveComponent *otherComp,
						int32 otherBodyIndex, bool bFromSweep, const FHitResult &sweepResult);

	UFUNCTION()
	void Launch();

	virtual void Tick(float DeltaTime) override;

protected:
	virtual void BeginPlay() override;

};
