// Fill out your copyright notice in the Description page of Project Settings.

#include "Launchpad.h"
#include "Classes/Components/BoxComponent.h"
#include "Classes/Components/StaticMeshComponent.h"
#include "Classes/Materials/MaterialInstanceDynamic.h"
#include "ConstructorHelpers.h"
#include "Classes/Kismet/GameplayStatics.h"
#include "Classes/Engine/World.h"
#include "Classes/GameFramework/Character.h"
#include "Classes/Components/AudioComponent.h"
#include "Classes/Sound/SoundBase.h"
#include "Classes/Components/ArrowComponent.h"

ALaunchpad::ALaunchpad()
{
 	PrimaryActorTick.bCanEverTick = true;

	// Setup Static Mesh
	this->meshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh1"));
	static ConstructorHelpers::FObjectFinder<UStaticMesh> meshAsset(TEXT("StaticMesh'/Game/Geometry/Meshes/1M_Cube.1M_Cube'"));
	if (meshAsset.Succeeded())
	{
		this->meshComp->SetStaticMesh(meshAsset.Object);	
	}

	this->meshComp->SetRelativeScale3D(FVector(1.0f, 1.0f, 0.1f));
	RootComponent = this->meshComp;

	// Apply Launchpad Material
	static ConstructorHelpers::FObjectFinder<UMaterial> foundMaterial(TEXT("Material'/Game/Geometry/Meshes/LaunchpadMaterial.LaunchpadMaterial'"));
	if (foundMaterial.Succeeded())
	{
		this->meshMaterial = foundMaterial.Object;
	}

	this->meshComp->SetMaterial(0, this->meshMaterial);

	// Setup Arrow Component for Launch Direction
	this->arrowComp = CreateDefaultSubobject<UArrowComponent>(TEXT("Arrow1"));
	this->arrowComp->SetRelativeRotation(FRotator(90.0f, 0.0f, 0.0f));
	this->arrowComp->SetupAttachment(this->meshComp);

	// Setup Box Component for Launch Collision detection
	this->boxComp = CreateDefaultSubobject<UBoxComponent>(TEXT("Box1"));
	this->boxComp->InitBoxExtent(FVector(50.0f, 50.0f, 100.0f));
	this->boxComp->SetupAttachment(this->meshComp);
	this->boxComp->OnComponentBeginOverlap.AddDynamic(this, &ALaunchpad::OnOverlapBegin);
	
	// Set Default Value for the Launch Velocity
	this->launchVelocity = 1000.0f;

	// Get the Sound to be played on Launch
	static ConstructorHelpers::FObjectFinder<USoundBase> launchSound(TEXT("SoundWave'/Game/FirstPerson/Audio/FirstPersonTemplateWeaponFire02.FirstPersonTemplateWeaponFire02'"));
	if (launchSound.Succeeded())
	{
		this->soundBase = launchSound.Object;
	}
}

void ALaunchpad::OnOverlapBegin(class UPrimitiveComponent* overlappedComp,
	class AActor *otherActor, class UPrimitiveComponent *otherComp,
	int32 otherBodyIndex, bool bFromSweep, const FHitResult &sweepResult)
{
	if (otherActor && (otherActor != this) && otherComp)
	{
		this->Launch();
	}
}

void ALaunchpad::Launch()
{
	ACharacter *playerChar = UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);

	UGameplayStatics::PlaySoundAtLocation(this, this->soundBase, this->GetActorLocation());

	FVector launchVector = this->arrowComp->GetForwardVector() * this->launchVelocity;

	playerChar->LaunchCharacter(launchVector, false, true);
}

void ALaunchpad::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ALaunchpad::BeginPlay()
{
	Super::BeginPlay();
	
}
