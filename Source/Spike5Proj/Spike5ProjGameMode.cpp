// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "Spike5ProjGameMode.h"
#include "Spike5ProjHUD.h"
#include "Spike5ProjCharacter.h"
#include "UObject/ConstructorHelpers.h"

ASpike5ProjGameMode::ASpike5ProjGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = ASpike5ProjHUD::StaticClass();
}
