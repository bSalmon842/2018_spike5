// Fill out your copyright notice in the Description page of Project Settings.

#include "Teleporter.h"
#include "Classes/Components/BoxComponent.h"
#include "Classes/Components/StaticMeshComponent.h"
#include "Classes/Materials/MaterialInstanceDynamic.h"
#include "ConstructorHelpers.h"
#include "Classes/Kismet/GameplayStatics.h"
#include "Classes/Engine/World.h"
#include "Classes/GameFramework/Character.h"
#include "Classes/Components/ArrowComponent.h"

ATeleporter::ATeleporter()
{
	PrimaryActorTick.bCanEverTick = true;

	// Setup Static Mesh
	this->meshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh1"));
	static ConstructorHelpers::FObjectFinder<UStaticMesh> meshAsset(TEXT("StaticMesh'/Game/Geometry/Meshes/1M_Cube.1M_Cube'"));
	if (meshAsset.Succeeded())
	{
		this->meshComp->SetStaticMesh(meshAsset.Object);
	}

	this->meshComp->SetRelativeScale3D(FVector(1.0f, 1.0f, 0.1f));
	RootComponent = this->meshComp;

	// Apply Launchpad Material
	static ConstructorHelpers::FObjectFinder<UMaterial> foundMaterial(TEXT("Material'/Game/Geometry/Meshes/TeleporterMaterial.TeleporterMaterial'"));
	if (foundMaterial.Succeeded())
	{
		this->meshMaterial = foundMaterial.Object;
	}

	this->meshComp->SetMaterial(0, this->meshMaterial);

	// Setup Arrow Component for Launch Direction
	this->arrowComp = CreateDefaultSubobject<UArrowComponent>(TEXT("Arrow1"));
	this->arrowComp->SetRelativeRotation(FRotator(90.0f, 0.0f, 0.0f));
	this->arrowComp->SetupAttachment(this->meshComp);

	// Setup Box Component for Launch Collision detection
	this->boxComp = CreateDefaultSubobject<UBoxComponent>(TEXT("Box1"));
	this->boxComp->InitBoxExtent(FVector(50.0f, 50.0f, 100.0f));
	this->boxComp->SetupAttachment(this->meshComp);
	this->boxComp->OnComponentBeginOverlap.AddDynamic(this, &ATeleporter::OnOverlapBegin);
}

void ATeleporter::OnOverlapBegin(class UPrimitiveComponent* overlappedComp,
	class AActor *otherActor, class UPrimitiveComponent *otherComp,
	int32 otherBodyIndex, bool bFromSweep, const FHitResult &sweepResult)
{
	if (otherActor && (otherActor != this) && otherComp)
	{
		this->Teleport();
	}
}

void ATeleporter::Teleport()
{
	ACharacter *playerChar = UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);

	FVector teleportLocation = this->arrowComp->K2_GetComponentLocation();

	playerChar->TeleportTo(teleportLocation, playerChar->GetViewRotation());
}

void ATeleporter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ATeleporter::BeginPlay()
{
	Super::BeginPlay();

}

