---
Spike Report
---

Core Spike 5
============

Introduction
------------

In order to understand how C++ Programming in Unreal Engine 4 works, a C++ version of 
our Blueprints example game from Core Spike 2 will be made, using C++ Classes instead
of Unreal's Blueprints, and will also include an extra mechanic.

Bitbucket Link: <https://bitbucket.org/bSalmon842/2018_spike5>

Goals
-----

1.	In a new C++ FPS Unreal Engine project, create a �Launch-pad� actor, which can
	be placed to cause the player to leap into the air upon stepping on it.
	
	i.	Play a sound when the character is launched
	
	ii.	Set the Launch Velocity using a variable, which lets you place multiple
		launch-pads with different velocities in the same level
	
	iii.	Add an Arrow Component, and use its rotation to define the direction
		to launch the character

2.	Create a teleporter with an Arrow Component to designate where the player will
	be teleported

Personnel
---------

  ------------------------- ------------------
  Primary -- Brock Salmon   Secondary -- N/A
  ------------------------- ------------------

Technologies, Tools, and Resources used
---------------------------------------

-   <https://docs.unrealengine.com/latest/INT/Engine/Blueprints/QuickStart/index.html>

-	Core Spike 2

Tasks undertaken
----------------

1.  Setup an Unreal Default C++ FPS Project

2. 	In a Launchpad C++ Class create an empty function called Launch for later
	and set a launchVelocity variable to 1000.0f as a default

3.  In the Launchpad Class, setup a UProperty Box Component for detecting
	when the player is on the launch pad with all initialisation in the
	constructor and make OnOverlapBegin call the Launch function.

4.  Create a Static Mesh Component and in the Constructor setup the component
	to use the 1m Cube Mesh.

5.  To identify the Launchpad create a new material, in this case Yellow

6. 	Apply the material to the Static Mesh using the "SetMaterial" member 
	function in the Static Mesh

7.  In the Launch function get the Player Character using
	UGameplayStatics::GetPlayerCharacter(), create an FVector multiplied by the
	launchVelocity variable to get a vertical direction and then use
	LaunchCharacter to make the Launchpad function.

8.  Add a SoundBase object as a variable to hold the Launchpad Sound

9. 	Setup the SoundBase variable with a sound to be played (in this case
	the FPS Projectile sound is used) and use UGameplayStatics::PlaySoundAtLocation()
	in the Launch function.

10. Setup an Arrow Component to dictate direction of the launch, by default it's best
	for it to be rotated to be pointing directly up.
	
11.	In the Launch function replace the FVector that is being multiplied with
	launchVelocity with the GetForwardVector() function in the Arrow Component so
	the Player will be launched in the direction of the arrow shown in the editor.

12. To Create the Teleporter create another C++ Class and copy the Launchpad code
	into the Teleporter class (as most of the code is applicable to the Teleporter).
	
13.	In bother the Header and Cpp file remove all instances of the soundBase and
	launchVelocity variables as they are no longer needed. Also rename the Launch
	function to 'Teleport'
	
14. Duplicate the Launchpad Material and change the colour (in this case to Blue)
	and apply that to the Static Mesh as was done for the Launchpad
	
15. In the Teleport function Get the Player Character (as before), then create an
	FVector to designate the location the player will be teleported to, set it to
	the Arrow Component's Location (using K2_GetComponentLocation()).
	
16.	Replace the LaunchCharacter call with TeleportTo(), and use the new teleport
	location variable
	
17. In the editor place down a teleporter and select the Arrow Component, move it
	to somewhere else in the level and test to ensure it is working correctly.

What we found out
-----------------

In this Spike we learned how to create C++ Classes in Unreal Engine's C++, as well
as how to find functions from Blueprints in C++ to replicate functionality with
more control. This gives us a better understanding of how Blueprints themselves
are processed as well as how to better traverse Unreal Engine's documentation.
For those new to C++ this could also improve their understanding of how OOP
is used in Unreal and other Games, as understanding how to use each class is
needed in this spike.
